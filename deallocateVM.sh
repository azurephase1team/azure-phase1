#!/usr/bin/bash

PMCAzure=$HOME/.PMCAzure

if [ ! -d $PMCAzure ]; then
    mkdir $PMCAzure
fi

# Use separate log file for each important step.

AzureCliInstallLog=$PMCAzure/AzureCliInstallLog
AzureLoginLog=$PMCAzure/PMCAzureLoginLog
AzureAccountLog=$PMCAzure/PMCAzureAccountLog
AzureAppLog=$PMCAzure/PMCAzureAppLog
AzureServicePrincipalLog=$PMCAzure/PMCAzureServicePrincipalLog
AzureRoleLog=$PMCAzure/PMCAzureRoleLog
AzureRoleMapLog=$PMCAzure/PMCAzureRoleMapLog

AzureRolePermsFile=$PMCAzure/PMCExampleAzureRole.json

# Install nodejs and npm if they aren't installed

NodeStatus=`node -v 2>&1`

if [[ $NodeStatus =~ .*command* ]]; then
    echo "Installing nodejs and npm"
    curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash - >> $AzureCliInstallLog 2>&1
    sudo apt-get install -y nodejs >> $AzureCliInstallLog 2>&1
    echo
fi

# Install Azure CLI (if it doesn't exist already)
# Need to figure out how to determine if azure-cli is installed

AzureStatus=`azure -v 2>&1`

if [[ $AzureStatus =~ .*command.* ]]; then
    # echo "Installing azure-cli"
    sudo npm install -g azure-cli >> $AzureCliInstallLog 2>&1
    # echo
fi

# Login to Azure
# Prompt for username. 
# - Can't be NULL
# - Trap failed login and prompt user to try again


    # azure login --username 4ffe8831-ab30-4601-8c1a-b6a6437db283 --password password --service-principal --tenant a29beaae-f439-4a80-ae9a-8c07fa9a9c4a

    azure login --username $1 --password $2 --service-principal --tenant $3 > $AzureLoginLog
 	


 # echo "hello"
 	# azure vm deallocate -g CLOUDNOSYS -n CloudNoSys --json
 	
 	azure vm deallocate -g $4 -n $5 --json

 	# azure vm start -g $4 -n $5 --json


