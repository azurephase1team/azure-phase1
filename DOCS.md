# 2-cloud-nosys-api-2 v0.0.0



- [Auth](#auth)
	- [Authenticate](#authenticate)
	
- [User](#user)
	- [Create user](#create-user)
	- [Deallocate VM](#deallocate-vm)
	- [Delete user](#delete-user)
	- [Get Rate Card Data](#get-rate-card-data)
	- [Retrieve current user](#retrieve-current-user)
	- [Retrieve User&#39;s resources](#retrieve-user&#39;s-resources)
	- [Start VM](#start-vm)
	- [Stop VM](#stop-vm)
	- [Update password](#update-password)
	- [Update user](#update-user)
	- [Get Usage Data](#get-usage-data)
	


# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|
| email			| String			|  <p>User's email.</p>							|
| tenantID			| String			|  <p>User's Azure tenant ID.</p>							|
| appID			| String			|  <p>User's Azure app ID.</p>							|
| appPWD			| String			|  <p>User's Azure app password/secret.</p>							|
| subscriptionID			| String			|  <p>User's Azure subscription ID.</p>							|
| password			| String			|  <p>User's password.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| role			| String			| **optional** 							|

## Deallocate VM



	GET /users/resources/stop


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			|  <p>Azure Resource Name.</p>							|
| group			| String			|  <p>Azure Resource Group.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Get Rate Card Data



	GET /users/rateCard


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve User&#39;s resources



	GET /users/resources


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Start VM



	GET /users/resources/start


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			|  <p>Azure Resource Name.</p>							|
| group			| String			|  <p>Azure Resource Group.</p>							|

## Stop VM



	GET /users/resources/stop


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			|  <p>Azure Resource Name.</p>							|
| group			| String			|  <p>Azure Resource Group.</p>							|

## Update password



	PUT /users/:id/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|

## Get Usage Data



	GET /users/usage


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|


