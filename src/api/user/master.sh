#!/bin/bash
# TBH i don't really know what I am doing
# But this should be an Azure CLI script iA
# danyal@cloudnosys.onmicrosoft.com
#  financeforall@outlook.com
# abdullah@provisioncapital.com
#  davefranco786@outlook.com


# azure login --username 4ffe8831-ab30-4601-8c1a-b6a6437db283 --password password --service-principal --tenant a29beaae-f439-4a80-ae9a-8c07fa9a9c4a


# Initialize Parameters
# Create hidden directory for stuff if it doesn't exist

PMCAzure=$HOME/.PMCAzure

if [ ! -d $PMCAzure ]; then
    mkdir $PMCAzure
fi

# Use separate log file for each important step.

AzureCliInstallLog=$PMCAzure/AzureCliInstallLog
AzureLoginLog=$PMCAzure/PMCAzureLoginLog
AzureAccountLog=$PMCAzure/PMCAzureAccountLog
AzureAppLog=$PMCAzure/PMCAzureAppLog
AzureServicePrincipalLog=$PMCAzure/PMCAzureServicePrincipalLog
AzureRoleLog=$PMCAzure/PMCAzureRoleLog
AzureRoleMapLog=$PMCAzure/PMCAzureRoleMapLog

AzureRolePermsFile=$PMCAzure/PMCExampleAzureRole.json

# Install nodejs and npm if they aren't installed

NodeStatus=`node -v 2>&1`

if [[ $NodeStatus =~ .*command* ]]; then
    echo "Installing nodejs and npm"
    curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash - >> $AzureCliInstallLog 2>&1
    sudo apt-get install -y nodejs >> $AzureCliInstallLog 2>&1
    echo
fi

# Install Azure CLI (if it doesn't exist already)
# Need to figure out how to determine if azure-cli is installed

AzureStatus=`azure -v 2>&1`

if [[ $AzureStatus =~ .*command.* ]]; then
    echo "Installing azure-cli"
    sudo npm install -g azure-cli >> $AzureCliInstallLog 2>&1
    echo
fi

# Login to Azure
# Prompt for username. 
# - Can't be NULL
# - Trap failed login and prompt user to try again

while [ -z "$status" ];
do

    echo "Logging into Azure."
    echo

    while [ -z $appID ]; 
    do
        read -p "Enter your Azure appID, appPWD, and TenantID  : " appID appPWD TenantID
    done
    
    # azure login --username 4ffe8831-ab30-4601-8c1a-b6a6437db283 --password password --service-principal --tenant a29beaae-f439-4a80-ae9a-8c07fa9a9c4a

    azure login --username $appID --password $appPWD --service-principal --tenant $TenantID > $AzureLoginLog
    echo 

    status=`grep OK $AzureLoginLog`

    if [ -z "$status" ]; then
       Username=""
    fi

done

# # Determine which subscription
# echo "Here are the subscriptions associated with your account:"
# echo
# cat $AzureLoginLog | awk -F" " '/subscription/ {print $4}'
# echo

# while [ -z $SubName ]; 
# do
#     read -p "Enter the subscription name you want to use: " SubName
#     echo
# done

# Determine which subscription
echo "Here are the vm's associated with your account:"
echo
Azure vm list --json
echo
read -p "please enter 1 to start a resource and 2 to stop a resource: " StartStop

    if [ "$StartStop" == "1" ];then
        echo
        Azure vm list
        echo
        while [ -z $resourcegroup ]; 
            do
                read -p "Enter the site name you want to Start: " resourcegroup name
                echo
            done
                azure vm start $name
                # azure site list | grep $SubName | awk '{system("azure site start "$2)}'
                echo
    else
        echo
        Azure vm list
        echo
        while [ -z $resourcegroup ]; 
            do
                read -p "Enter the vm resourcegroup and name you want to deallocate: " resourcegroup name
                echo
            done
                azure vm deallocate $name
                # azure vm list | grep $SubName | awk '{system("azure site stop "$2)}'
                echo
    fi
echo
Azure vm list
echo


# while [ -z $SubName ]; 
# do
#     read -p "Enter the site name you want to use: " SubName
#     echo
# done

    
# # Get subscription and tenant ID's
# azure account show -s $SubName > $AzureAccountLog

# SubscriptionID=`grep "ID" $AzureAccountLog | grep -v Tenant | awk -F": " '{print $3}' | xargs`
# TenantID=`grep "Tenant ID" $AzureAccountLog | awk -F": " '{print $3}' | xargs`
