import _ from 'lodash'
import { success, notFound } from '../../services/response/'
import { User } from '.'

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  User.find(query, select, cursor)
    .then((users) => users.map((user) => user.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then((user) => user ? user.view() : null)
    .then(success(res))
    .catch(next)

export const showMe = ({ user }, res) =>
  res.json(user.view(true))

export const showResources = ({ querymen, user }, res) =>
  user.resourceview(true, querymen.query.subscription, function (returnedData) {
    res.send(returnedData)
    console.log('in the controller', returnedData)
    console.log('in the controller', querymen.query.subscription)
  })

export const rateCard = ({ user }, res) =>
  user.rateCard(true, function (returnedData) {
    res.send(returnedData)
    console.log('in reteCard in controller', returnedData)
  })

export const dailyUsagebyDate = ({ user }, res) =>
  user.dailyUsagebyDate(true, function (returnedData) {
    res.send(returnedData)
    console.log('in dailyUsagebyDate in controller', returnedData)
  })

export const deallocateResource = ({ bodymen: { body }, user }, res) =>
  user.deallocate(body, user, function (returnedData) {
    res.send(returnedData.statusCode)
    console.log('in the controller', returnedData.statusCode)
  })

export const stopResource = ({ bodymen: { body }, user }, res) =>
  user.stop(body, user, function (returnedData) {
    res.send(returnedData.statusCode)
    console.log('in the controller', returnedData.statusCode)
  })

export const startResource = ({ bodymen: { body }, user }, res) =>
  user.start(body, user, function (returnedData) {
    res.send(returnedData)
    console.log('in the controller', returnedData)
  })
  

export const create = ({ bodymen: { body } }, res, next) =>
  User.create(body)
    .then((user) => user.view(true))
    .then(success(res, 201))
    .catch((err) => {
      /* istanbul ignore else */
      if (err.name === 'MongoError' && err.code === 11000) {
        res.status(409).json({
          valid: false,
          param: 'email',
          message: 'email already registered'
        })
      }
      else if (err.name === "ValidationError") {
        // console.log(err)
        res.status(401).json({
          valid: false,
          param: 'azure creds',
          message: 'invalid azure credentials'
        })
      } else {
        next(err)
      }
    })

// export const verifyCreds = ({ bodymen: { body } }, res, next) =>
//   User.verifyCreds(body, function (returnedData) {
//     console.log('in the verifyCreds controller', returnedData)
//     // if (err.name === 'MongoError' && err.code === 11000) {
//     //     res.status(409).json({
//     //       valid: false,
//     //       param: 'email',
//     //       message: 'email already registered'
//     //     })
//     // } 
//     // else {
//     //     next(err)
//     // }
//   })

export const update = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isAdmin = user.role === 'admin'
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate && !isAdmin) {
        res.status(401).json({
          valid: false,
          message: 'You can\'t change other user\'s data'
        })
        return null
      }
      return result
    })
    .then((user) => user ? _.merge(user, body).save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(success(res))
    .catch(next)

export const updatePassword = ({ bodymen: { body }, params, user }, res, next) =>
  User.findById(params.id === 'me' ? user.id : params.id)
    .then(notFound(res))
    .then((result) => {
      if (!result) return null
      const isSelfUpdate = user.id === result.id
      if (!isSelfUpdate) {
        res.status(401).json({
          valid: false,
          param: 'password',
          message: 'You can\'t change other user\'s password'
        })
        return null
      }
      return result
    })
    .then((user) => user ? user.set({ password: body.password }).save() : null)
    .then((user) => user ? user.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  User.findById(params.id)
    .then(notFound(res))
    .then((user) => user ? user.remove() : null)
    .then(success(res, 204))
    .catch(next)
