import crypto from 'crypto'
import bcrypt from 'bcrypt'
import mongoose, { Schema } from 'mongoose'
import mongooseKeywords from 'mongoose-keywords'
import { env } from '../../config'
// var spawn   = require('child_process').spawn;
var express = require('express');
var request = require('request');

const roles = ['user', 'admin']

const userSchema = new Schema({
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: true,
    unique: true,
    trim: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true,
    minlength: 6
  },
  name: {
    type: String,
    index: true,
    trim: true
  },
  role: {
    type: String,
    enum: roles,
    default: 'user'
  },
  subscriptionID: {
    type: String,
    trim: true,
    required: true
  },
  customerId: {
    type: String,
    trim: true,
    required: true
  },
  picture: {
    type: String,
    trim: true
  },
  tenantID: {
    type: String,
    trim: true,
    required: true,
    minlength: 6
  },
  appID: {
    type: String,
    trim: true,
    required: true
  },
  appPWD: {
    type: String,
    trim: true,
    required: true
  },
  provider: {
    type: String,
    trim: true,
    required: true
  },
  accessKeyId: {
    type: String
  },
  secretAccessKey: {
    type: String
  },


}, {
  timestamps: true
})

userSchema.path('email').set(function (email) {
  if (!this.picture || this.picture.indexOf('https://gravatar.com') === 0) {
    const hash = crypto.createHash('md5').update(email).digest('hex')
    this.picture = `https://gravatar.com/avatar/${hash}?d=identicon`
  }

  if (!this.name) {
    this.name = email.replace(/^(.+)@.+$/, '$1')
  }

  return email
})



function AzureARMAccessToken(ClientID, ClientSecret, TennantID, callback) {
  var options = {
      url: 'https://login.windows.net/' + TennantID + '/oauth2/token', //URL to hit
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'accept': 'application/json'
      },
      body: 'resource=' + encodeURIComponent('https://management.core.windows.net/') + '&client_id=' + ClientID + '&grant_type=client_credentials&client_secret=' + encodeURIComponent(ClientSecret),

  };
  //Start the request
  request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
          callback(body);
          // console.log('!error', response, body);
          
      }
      else{
          // console.log('after acces token req', error, body);
          callback(body);
      }

  });
}

userSchema.pre('save', function (next) {

  if (!this.isModified('password')) return next()
  
  /* istanbul ignore next */
  const rounds = env === 'test' ? 1 : 9
   var self = this
   this.name = this.name.trim()
   this.tenantID = this.tenantID.trim()
   this.appID = this.appID.trim()
   this.appPWD = this.appPWD.trim()
   this.subscriptionID = this.subscriptionID.trim()
   this.customerId =  this.subscriptionID.trim()
   this.provider = 2
   this.accessKeyId = this.subscriptionID.trim()
   this.secretAccessKey = this.appID.trim()
   console.log("------------pre save=======")
          bcrypt.hash(self.password, rounds).then((hash) => {
            self.password = hash
            next()
          }).catch(next)
      

   

})

userSchema.pre('validate', function (next) {
    console.log(next)
   var self = this
   this.name = this.name.trim()
   this.tenantID = this.tenantID.trim()
   this.appID = this.appID.trim()
   this.appPWD = this.appPWD.trim()
   this.subscriptionID = this.subscriptionID.trim()
   this.customerId =  this.subscriptionID.trim()
   this.provider = 2
   this.accessKeyId = this.subscriptionID.trim()
   this.secretAccessKey = this.appID.trim()

   AzureARMAccessToken(this.appID, this.appPWD, this.tenantID, function (returndata) {
       var jsonData = JSON.parse(returndata);
       if (jsonData.error) {
        var err = jsonData.error
        self.tenantID = null
        next(err)
       }else{
           next()
       }
   })
})








userSchema.methods = {
  view (full) {
    let view = {}
    let fields = ['id', 'name', 'picture','subscriptionID', 'tenantID' ]

    if (full) {
      fields = [...fields, 'email', 'createdAt']
    }

    fields.forEach((field) => { view[field] = this[field] })

    return view
  },
  deallocate (vm, user, callback) {
    let view = {}
    let fields = ['id', 'name', 'picture','subscriptionID', 'tenantID', 'appID', 'appPWD' ]

    if (user) {
      fields = [...fields, 'email', 'createdAt']
    }
    fields.forEach((field) => { view[field] = this[field] });
    


    function StopVM(resourceGroup, vm, subscriptionID, clientID, clientSecret, tennantID,) {
        AzureARMAccessToken(clientID, clientSecret, tennantID, function (data) {
            var jsonData = JSON.parse(data);
            var accessToken = 'bearer ' + jsonData.access_token
            // console.log('token', accessToken)
            //Get StorageAccount info
            var options = {
                // url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/resourceGroups/' + resourceGroup + '/providers/Microsoft.Compute/virtualMachines/' + vm +'/start?api-version=2016-04-30-preview',
                url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/resourceGroups/' + resourceGroup + '/providers/Microsoft.Compute/virtualMachines/' + vm +'/deallocate?api-version=2016-04-30-preview',
                method: 'POST',
                headers: {
                    'Authorization': accessToken,
                    'Content-Type': 'application/json'
                },
                json: true,
                body: {
                    'subscriptionId': subscriptionID,
                    'resourceGroup': resourceGroup,
                    'vm': vm
                }
            };
            //Start the request
            request(options, function (error, response) {
                if (!error) {
                    
                    callback(response)
                    // console.log('response', response);
                }
                else{
                    callback(error)
                    // console.log('error', error);
                }

            });

        })
    }

    //Main
    var myResourceGroup = vm.group;
    var myVM = vm.name;
    var mySubscriptionID = vm.subscription
    var myClientID = view.appID;
    var myClientSecret = view.appPWD;
    var myTennantID = view.tenantID;


    StopVM(myResourceGroup, myVM, mySubscriptionID, myClientID, myClientSecret, myTennantID,);

    // var spawn = require('child_process').spawn;
    // // var ls = spawn('ls');
    // let ls = spawn('sh', [ 'deallocateVM.sh', view.appID, view.appPWD, view.tenantID, vm.group, vm.name ]);
    // let list = {}

    // ls.stdout.on('data', (data) => {
    //   // console.log(`stdout: ${data}`);
    //   list.stdout = `${data}`
    //   console.log('on ls.stdout', list.stdout);
      
    // });

    // ls.stderr.on('data', (data) => {
    //   console.log(`stderr: ${data}`);
    //   list.err = data
    // });

    // ls.on('close', (code) => {
    //   callback(list.stdout)
    //   console.log(`child process exited with code ${code}`, list);
    //   // return view
    // });
  },
  stop (vm, user, callback) {
    
    let view = {}
    let fields = ['id', 'name', 'picture','subscriptionID', 'tenantID', 'appID', 'appPWD' ]

    if (user) {
      fields = [...fields, 'email', 'createdAt']
    }
    fields.forEach((field) => { view[field] = this[field] });
    


    function StopVM(resourceGroup, vm, subscriptionID, clientID, clientSecret, tennantID,) {
        AzureARMAccessToken(clientID, clientSecret, tennantID, function (data) {
            var jsonData = JSON.parse(data);
            var accessToken = 'bearer ' + jsonData.access_token
            console.log('token', accessToken)
            //Get StorageAccount info
            var options = {
                // url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/resourceGroups/' + resourceGroup + '/providers/Microsoft.Compute/virtualMachines/' + vm +'/start?api-version=2016-04-30-preview',
                url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/resourceGroups/' + resourceGroup + '/providers/Microsoft.Compute/virtualMachines/' + vm +'/powerOff?api-version=2016-04-30-preview',
                method: 'POST',
                headers: {
                    'Authorization': accessToken,
                    'Content-Type': 'application/json'
                },
                json: true,
                body: {
                    'subscriptionId': subscriptionID,
                    'resourceGroup': resourceGroup,
                    'vm': vm
                }
            };
            //Start the request
            request(options, function (error, response) {
                if (!error) {
                    
                    callback(response)
                    // console.log('response', response);
                }
                else{
                    callback(error)
                    // console.log('error', error);
                }

            });

        })
    }

    //Main
    var myResourceGroup = vm.group;
    var myVM = vm.name;
    var mySubscriptionID = vm.subscription
    var myClientID = view.appID;
    var myClientSecret = view.appPWD;
    var myTennantID = view.tenantID;


    StopVM(myResourceGroup, myVM, mySubscriptionID, myClientID, myClientSecret, myTennantID,);

  },

  start (vm, user, callback) {

    let view = {}
    let fields = ['id', 'name', 'picture','subscriptionID', 'tenantID', 'appID', 'appPWD' ]

    if (user) {
      fields = [...fields, 'email', 'createdAt']
    }
    fields.forEach((field) => { view[field] = this[field] });

    
    function StartVM(resourceGroup, vm, subscriptionID, clientID, clientSecret, tennantID,) {
        AzureARMAccessToken(clientID, clientSecret, tennantID, function (data) {
            var jsonData = JSON.parse(data);
            var accessToken = 'bearer ' + jsonData.access_token
            console.log('token', accessToken)
            //Get StorageAccount info
            var options = {
                url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/resourceGroups/' + resourceGroup + '/providers/Microsoft.Compute/virtualMachines/' + vm +'/start?api-version=2016-04-30-preview',
                // url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/resourceGroups/' + resourceGroup + '/providers/Microsoft.Compute/virtualMachines/' + vm +'/deallocate?api-version=2016-04-30-preview',
                method: 'POST',
                headers: {
                    'Authorization': accessToken,
                    'Content-Type': 'application/json'
                },
                json: true,
                body: {
                    'subscriptionId': subscriptionID,
                    'resourceGroup': resourceGroup,
                    'vm': vm
                }
            };
            //Start the request
            request(options, function (error, response) {
                if (!error) {
                    
                    callback(response.body)
                    console.log('-------------------------------------response', response.body);
                }
                else{
                    callback("error.body")
                    console.log('error');
                }

            });

        })
    }

    //Main
    var myResourceGroup = vm.group;
    var myVM = vm.name;
    var mySubscriptionID = vm.subscription
    // var mySubscriptionID = view.subscriptionID
    var myClientID = view.appID;
    var myClientSecret = view.appPWD;
    var myTennantID = view.tenantID;


    StartVM(myResourceGroup, myVM, mySubscriptionID, myClientID, myClientSecret, myTennantID,);




    // var spawn = require('child_process').spawn;
    // // var ls = spawn('ls');
    // let ls = spawn('sh', [ 'startVM.sh', view.appID, view.appPWD, view.tenantID, vm.group, vm.name ]);
    // let list = {}

    // ls.stdout.on('data', (data) => {
    //   // console.log(`stdout: ${data}`);
    //   list.stdout = `${data}`
    //   callback(list.stdout)
    //   console.log('on ls.stdout', list.stdout);
      
    // });

    // ls.stderr.on('data', (data) => {
    //   list.err = data
      
    //   console.log(`stderr: ${data}`);
    // });

    // ls.on('close', (code) => {
    //   if (list.err) {
    //     callback(list.err)
    //   }
    //   else {
    //     callback(list.stdout)
    //   }
    //   console.log(`child process exited with code ${code}`, list);
    //   // return view
    // });
  },

  resourceview (full, subsc, cb) {
    let view = {}
    let fields = ['id', 'name', 'picture','subscriptionID', 'tenantID', 'appID', 'appPWD' ]

    if (full) {
      fields = [...fields, 'email', 'createdAt']
    }
    fields.forEach((field) => { view[field] = this[field] });

    //Function to Get Azure Subscription Information
    function ListResourcesinSubscription(clientID, clientSecret, tennantID, subscriptionID) {
        AzureARMAccessToken(clientID, clientSecret, tennantID, function (data) {
            var jsonData = JSON.parse(data);
            var accessToken = 'bearer ' + jsonData.access_token
            //Get StorageAccount info
            var options = {
                url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/providers/Microsoft.Compute/virtualmachines?api-version=2016-04-30-preview',
                // url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/resources?api-version=2016-09-01&$filter=Microsoft.Compute/virtualMachines',
                method: 'GET',
                headers: {
                    'Authorization': accessToken,
                    'accept': 'application/json'
                },
            };
            //Start the request
            request(options, function (error, response, body) {
              console.log('body', body)
                if (!error && response.statusCode == 200) {
                    var jsonData = JSON.parse(body);
                    cb(jsonData)
                    console.log('success', jsonData);
                }
                else{
                    console.log('error', body);
                    cb(body)
                }
            });

        })
    }

    //Main
    var myClientID = view.appID;
    var myClientSecret = view.appPWD;
    var myTennantID = view.tenantID;
    var mySubscriptionID = subsc


    ListResourcesinSubscription(myClientID, myClientSecret, myTennantID, mySubscriptionID);

    
  },

  verifyCreds (full, cb) {
    let view = {}
    let fields = ['id', 'name', 'picture','subscriptionID', 'tenantID', 'appID', 'appPWD' ]

    if (full) {
      fields = [...fields, 'email', 'createdAt']
    }
    fields.forEach((field) => { view[field] = this[field] });

    //Function to Get Azure Subscription Information
    function ListResourcesinSubscription(clientID, clientSecret, tennantID, subscriptionID) {
        AzureARMAccessToken(clientID, clientSecret, tennantID, function (data) {
            var jsonData = JSON.parse(data);
            var accessToken = 'bearer ' + jsonData.access_token
            console.log('in the verifyCreds Model.js', jsonData)

        })
    }

    //Main
    var myClientID = view.appID;
    var myClientSecret = view.appPWD;
    var myTennantID = view.tenantID;
    var mySubscriptionID = view.subscriptionID


    ListResourcesinSubscription(myClientID, myClientSecret, myTennantID, mySubscriptionID);
 
    
  },

  dailyUsagebyDate (full, cb) {
    let view = {}
    let fields = ['id', 'name', 'picture','subscriptionID', 'tenantID', 'appID', 'appPWD' ]

    if (full) {
      fields = [...fields, 'email', 'createdAt']
    }
    fields.forEach((field) => { view[field] = this[field] });

    //Function to Get Azure Subscription Information
    function dailyUsagebyDateFunction(clientID, clientSecret, tennantID, subscriptionID) {
        AzureARMAccessToken(clientID, clientSecret, tennantID, function (data) {
            var jsonData = JSON.parse(data);
            var accessToken = 'bearer ' + jsonData.access_token
            //Get StorageAccount info
            var options = {
                // url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/providers/Microsoft.Compute/virtualmachines?api-version=2016-04-30-preview',
                url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/providers/Microsoft.Commerce/UsageAggregates?api-version=2015-06-01-preview&reportedStartTime='+ '2017-03-01' + 'T00%3a00%3a00%2b00%3a00&reportedEndTime='+'2017-05-01'+'T00%3a00%3a00%2b00%3a00&aggregationGranularity='+'Daily'+'&showDetails=true',
                // url: 'https://management.azure.com/subscriptions/' + subscriptionID + "/providers/Microsoft.Commerce/RateCard?api-version=2015-06-01-preview&$filter=OfferDurableId eq 'MS-AZR-0003P' and Currency eq 'USD' and Locale eq 'en-US' and RegionInfo eq 'US'",
                method: 'GET',
                headers: {
                    'Authorization': accessToken,
                    'Content-Type': 'application/json'
                },
            };
            //Start the request
            request(options, function (error, response, body) {
                console.log('body', body)
                
                if (!error && response.statusCode == 200) {
                    var jsonData = JSON.parse(body);
                    cb(jsonData)
                    console.log('success', jsonData);
                }
                else{
                    cb(error)
                    console.log('error', error);
                }
                
            });

        })
    }
    //Main
    var myClientID = view.appID;
    var myClientSecret = view.appPWD;
    var myTennantID = view.tenantID;
    var mySubscriptionID = view.subscriptionID


    dailyUsagebyDateFunction(myClientID, myClientSecret, myTennantID, mySubscriptionID);
    
    
  },

  rateCard (full, cb) {
    let view = {}
    let fields = ['id', 'name', 'picture','subscriptionID', 'tenantID', 'appID', 'appPWD' ]

    if (full) {
      fields = [...fields, 'email', 'createdAt']
    }
    fields.forEach((field) => { view[field] = this[field] });

    //Function to Get Azure Subscription Information
    function rateCardFunction(clientID, clientSecret, tennantID, subscriptionID) {
        AzureARMAccessToken(clientID, clientSecret, tennantID, function (data) {
            var jsonData = JSON.parse(data);
            var accessToken = 'bearer ' + jsonData.access_token
            //Get StorageAccount info
            var options = {
                // url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/providers/Microsoft.Compute/virtualmachines?api-version=2016-04-30-preview',
                // url: 'https://management.azure.com/subscriptions/' + subscriptionID + '/providers/Microsoft.Commerce/UsageAggregates?api-version=2015-06-01-preview&reportedStartTime=2017-03-01T00%3a00%3a00%2b00%3a00&reportedEndTime=2017-05-01T00%3a00%3a00%2b00%3a00&aggregationGranularity=Daily&showDetails=true',
                url: 'https://management.azure.com/subscriptions/' + subscriptionID + "/providers/Microsoft.Commerce/RateCard?api-version=2015-06-01-preview&$filter=OfferDurableId eq 'MS-AZR-0003P' and Currency eq 'USD' and Locale eq 'en-US' and RegionInfo eq 'US'",
                method: 'GET',
                headers: {
                    'Authorization': accessToken,
                    'Content-Type': 'application/json'
                },
            };
            //Start the request
            request(options, function (error, response, body) {
                console.log('body', body)
                
                if (!error && response.statusCode == 200) {
                    var jsonData = JSON.parse(body);
                    cb(jsonData)
                    console.log('success', jsonData);
                }
                else{
                    cb(error)
                    console.log('error', error);
                }
                
            });

        })
    }

    //Main
    var myClientID = view.appID;
    var myClientSecret = view.appPWD;
    var myTennantID = view.tenantID;
    var mySubscriptionID = view.subscriptionID


    rateCardFunction(myClientID, myClientSecret, myTennantID, mySubscriptionID);
    
    
  },


subscriptionview (full, cb) {
    let view = {}
    let fields = ['id', 'name', 'picture','subscriptionID', 'tenantID', 'appID', 'appPWD' ]

    if (full) {
      fields = [...fields, 'email', 'createdAt']
    }
    fields.forEach((field) => { view[field] = this[field] });

    function AzureARMAccessToken(ClientID, ClientSecret, TennantID, callback) {
      var options = {
          url: 'https://login.windows.net/' + TennantID + '/oauth2/token', //URL to hit
          method: 'POST',
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'accept': 'application/json'
          },
          body: 'resource=' + encodeURIComponent('https://management.core.windows.net/') + '&client_id=' + ClientID + '&grant_type=client_credentials&client_secret=' + encodeURIComponent(ClientSecret),

      };
      //Start the request
      request(options, function (error, response, body) {
          if (!error && response.statusCode == 200) {
              callback(body);
          }
          else
              {console.log(error);}


      });
    }


    //Function to Get Azure Subscription Information
    function GetAzureSubscription(clientID, clientSecret, tennantID, subscriptionID) {
        AzureARMAccessToken(clientID, clientSecret, tennantID, function (data) {
            var jsonData = JSON.parse(data);
            var accessToken = 'bearer ' + jsonData.access_token
            //Get StorageAccount info
            var options = {
                url: 'https://management.azure.com/subscriptions/' + subscriptionID + '?api-version=2016-09-01',
                method: 'GET',
                headers: {
                    'Authorization': accessToken,
                    'accept': 'application/json'
                },
            };
            //Start the request
            request(options, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    var jsonData = JSON.parse(body);
                    cb(jsonData)
                    console.log(jsonData);
                }
                else{
                    cb(error)
                    console.log(error);
                }

            });

        })
    }

    //Main
    var myClientID = view.appID;
    var myClientSecret = view.appPWD;
    var myTennantID = view.tenantID;
    var mySubscriptionID = view.subscriptionID


    GetAzureSubscription(myClientID, myClientSecret, myTennantID, mySubscriptionID);
    
  },





  authenticate (password) {
    return bcrypt.compare(password, this.password).then((valid) => valid ? this : false)
  }
}

userSchema.statics = {
  roles
}

userSchema.plugin(mongooseKeywords, { paths: ['email', 'name'] })

const model = mongoose.model('Customers', userSchema)

export const schema = model.schema
export default model
