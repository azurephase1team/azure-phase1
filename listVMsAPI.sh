#!/bin/bash

# bash script to retrieve Azure Subscription information using plain Azure ARM REST API web requests

#Azure Subscription variables
ClientID="4ffe8831-ab30-4601-8c1a-b6a6437db283" #ApplicationID
# ClientID="[ClientID]" #ApplicationID
ClientSecret="password"  #key from Application
# ClientSecret="[ClientSecret]"  #key from Application
TennantID="a29beaae-f439-4a80-ae9a-8c07fa9a9c4a"
# TennantID="[TennantID]"
SubscriptionID="4849aae5-c123-44ed-92de-7aba006efc41"
# SubscriptionID="[SubscriptionID]"

accesstoken=$(curl -s --header "accept: application/json" --request POST "https://login.windows.net/$TennantID/oauth2/token" --data-urlencode "resource=https://management.core.windows.net/" --data-urlencode "client_id=$ClientID" --data-urlencode "grant_type=client_credentials" --data-urlencode "client_secret=$ClientSecret" | jq -r '.access_token')

#Use AccessToken in Azure ARM REST API call for Subscription Info
subscriptionURI="https://management.azure.com/subscriptions/$SubscriptionID?api-version=2016-09-01"

curl -s --header "authorization: Bearer $accesstoken" --request GET $subscriptionURI | jq .